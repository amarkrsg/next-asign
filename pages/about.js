import LatestBlog from "../components/slider/LatestBlog";
import Slider from "react-slick";

function about({users}){
    const settings = {
        dots: false,
        arrows:true,
        autoplay:false,
        lazyLoad: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 2,
    };
    return(
        <div className="container">
            <h1>About</h1>
            <div className="slider-wrap">
                <Slider {...settings}>	
                    {                            
                        users.map((user)=>{
                            return(
                                <div key={user.id} className='p-2'>
                                    <LatestBlog user = {user} />
                                </div>
                            )
                        })                        
                    }	
                </Slider> 
            </div>
        </div>
    )
}

export default about;

export async function getStaticProps() {
	const res = await fetch('https://jsonplaceholder.typicode.com/photos?_page=1&_limit=10');
	const data = await res.json();

	return {
		props: {
			users:data,
		},
	}
}