// import LatestBlog from './slider/LatestBlog';
import React, { useEffect, useState } from "react";
import Link from "next/link";
import Slider from "react-slick";
import BannerSlider from '../components/slider/BannerSlider'; 
import LatestBlog from "../components/slider/LatestBlog";
import Image from 'next/image';

import ReactPaginate from 'react-paginate';
import SkeletonBlogHome from '../components/layout/skeleton/SkeletonBlogHome';

import useTranslation from "next-translate/useTranslation";

/**/
export async function getStaticProps() {
	const res = await fetch('https://jsonplaceholder.typicode.com/photos?_page=1&_limit=10');
	const data = await res.json();

	return {
		props: {
			users:data, 
		},
	}
}

// import LatestBlog from '../components/slider/LatestBlog';
function HomePage({users}) {

    const settings = {
        dots: false,
        arrows:true,
        autoplay:false,
        lazyLoad: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 2,
    };
    const [items, setItems] = useState([]);
    const [pageCount, setpageCount] = useState(0);
    const [isLoading, setIsLoading] = useState(true);

    let limit = 10;

  
    useEffect(() => {
        const getComments = async () => {
          const res = await fetch(
            `https://jsonplaceholder.typicode.com/photos?_page=1&_limit=${limit}`

          );
          const data = await res.json();

          setIsLoading(false);

          const total = res.headers.get("x-total-count");
          setpageCount(Math.ceil(total / limit));
          // console.log(Math.ceil(total/12));
          setItems(data);
        };
    
        getComments();
    }, [limit]);
    
    const fetchComments = async (currentPage) => {
        
        const res = await fetch(
          `https://jsonplaceholder.typicode.com/photos?_page=${currentPage}&_limit=${limit}`
        );
        
        const data = await res.json();
        return data;
        
    };
    
    const handlePageClick = async (data) => {
        setIsLoading(true);
        console.log(data.selected);
        let currentPage = data.selected + 1;
        const commentsFormServer = await fetchComments(currentPage);
        
        setItems(commentsFormServer);
        setIsLoading(false);
    };

    let { t } = useTranslation();
  return(

    <>
        <BannerSlider />
        <div className='container'>

            <div className='title-filter'>
                <h2>{t('common:blogs')} </h2>                 
                
            </div>
            <ul className="grid grid-5">
                {
                    isLoading ?
                        <>
                        <SkeletonBlogHome /><SkeletonBlogHome /><SkeletonBlogHome />
                        <SkeletonBlogHome /><SkeletonBlogHome /><SkeletonBlogHome />
                        <SkeletonBlogHome /><SkeletonBlogHome /><SkeletonBlogHome />
                        <SkeletonBlogHome /> </>
                        
                        :
                        items.map((items) => {
                            return (
                                <li key={items.id}>
                                    <div className="blogItem">
                                        <div className="blogImage">
                                            <Link href={"/blog/" + items.id}>
                                                <a><Image src={items.url} alt='' width="320" height="300" /></a>
                                            </Link>
                                        </div>
                                        <div className='blog-content'>
                                            <h3 className='blog-title'>
                                                <Link href={"/blog/" + items.id}>{items.title}</Link>
                                            </h3>
                                            <p>User {items.id}</p>
                                        </div>
                                    </div>
                                </li>
                            );
                        })
                    
                }                      
                
            </ul>       
                                
            <ReactPaginate
                previousLabel={"previous"}
                nextLabel={"next"}
                breakLabel={"..."}
                pageCount={pageCount}
                marginPagesDisplayed={2}
                pageRangeDisplayed={3}
                onPageChange={handlePageClick}
                containerClassName={"pagination justify-content-center"}
                pageClassName={"page-item"}
                pageLinkClassName={"page-link"}
                previousClassName={"page-item"}
                previousLinkClassName={"page-link"}
                nextClassName={"page-item"}
                nextLinkClassName={"page-link"}
                breakClassName={"page-item"}
                breakLinkClassName={"page-link"}
                activeClassName={"active"}
            />

          
            <h2>{t('common:latest_blog')} </h2>
            <div className="slider-wrap">
                <Slider {...settings}>	
                    {                            
                        users.map((user)=>{
                            return(
                                <div key={user.id} className='p-2'>
                                    <LatestBlog user = {user} />
                                </div>
                            )
                        })                        
                    }	
                </Slider> 
            </div>

        </div>
    </>    
  ) 
}

export default HomePage;