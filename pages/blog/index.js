import Image from 'next/image';
import styles from '../../styles/Blog.module.css';
import React, { useEffect, useState } from "react";
import Link from 'next/link';

import ReactPaginate from 'react-paginate';
import SkeletonBlogItem from '../../components/layout/skeleton/SkeletonBlogItem';

import useTranslation from "next-translate/useTranslation";


function Blog(props) {
    
    const [items, setItems] = useState([]);
    const [pageCount, setpageCount] = useState(0);
    const [isLoading, setIsLoading] = useState(true);

    let limit = 12;

    useEffect(() => {
        const getComments = async () => {
          const res = await fetch(
            `https://jsonplaceholder.typicode.com/photos?_page=1&_limit=${limit}`
          );
          const data = await res.json();
          setIsLoading(false);
          const total = res.headers.get("x-total-count");
          setpageCount(Math.ceil(total / limit));
          // console.log(Math.ceil(total/12));
          setItems(data);
        };
    
        getComments();
    }, [limit]);
    
    const fetchComments = async (currentPage) => {
        const res = await fetch(
          `https://jsonplaceholder.typicode.com/photos?_page=${currentPage}&_limit=${limit}`
        );
        const data = await res.json();
       
        return data;
    };
    
    const handlePageClick = async (data) => {
        console.log(data.selected);
        let currentPage = data.selected + 1;
        const commentsFormServer = await fetchComments(currentPage);
        setItems(commentsFormServer);
    };  

    /* Sorting  */

    let { t } = useTranslation();


    return (
        <div className="container">
            
            <div className={styles.titleFilter}>
                <h1>{t("common:blogs")} </h1>                 
                  
                <div className={styles.filter}>
                    <span>sort_by :- </span>
                    {/* <span>{ t('sort_by') } :- </span> */}
                    <select>
                        <option value="id">Id</option>
                        <option value="title">Name</option>
                    </select>
                </div>
            </div>

            <ul className="grid grid-4 row">
            {   
                isLoading ?
                <>
                <SkeletonBlogItem /> <SkeletonBlogItem />
                <SkeletonBlogItem /> <SkeletonBlogItem />
                <SkeletonBlogItem /> <SkeletonBlogItem />
                <SkeletonBlogItem /> <SkeletonBlogItem />
                <SkeletonBlogItem /> <SkeletonBlogItem />
                <SkeletonBlogItem /> <SkeletonBlogItem />
                </>
                :
                items.map((items) => {
                    return (
                        <li key={items.id} className="col-md-3">
                            <div className="blogItem">
                                <div className="blogImage">
                                    <Link href={"/blog/" + items.id}>
                                        <a><Image src={items.url} alt='img' width="320" height="300" /></a>
                                    </Link>
                                </div>
                                <div className='blog-content'>
                                    <h3 className='blog-title'>
                                        <Link href={"/blog/" + items.id}>{items.title}</Link>
                                    </h3>
                                    <p>User {items.id}</p>
                                </div>
                            </div>
                        </li>
                    );
                })
            } 
            </ul>
                   
                
                   
                
                
            <ReactPaginate
                previousLabel={"previous"}
                nextLabel={"next"}
                breakLabel={"..."}
                pageCount={pageCount}
                marginPagesDisplayed={2}
                pageRangeDisplayed={3}
                onPageChange={handlePageClick}
                containerClassName={"pagination justify-content-center"}
                pageClassName={"page-item"}
                pageLinkClassName={"page-link"}
                previousClassName={"page-item"}
                previousLinkClassName={"page-link"}
                nextClassName={"page-item"}
                nextLinkClassName={"page-link"}
                breakClassName={"page-item"}
                breakLinkClassName={"page-link"}
                activeClassName={"active"}
            />      
                
                    
           
        </div>
    );
}

export default Blog;