import Link from "next/link";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import ReactPaginate from 'react-paginate';
import { SkeletonTheme } from "react-loading-skeleton";
import SkeletonComment from '../../../components/layout/skeleton/SkeletonComment';
import styles from "../../../styles/Blog.module.css";

import useTranslation from "next-translate/useTranslation";

/**/
import Slider from "react-slick";

/**/


export const getStaticPaths = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/photos");
  const data = await res.json();
  const paths = data.map((curElem) => {
    return {
      params: {
        blogId: curElem.id.toString(),
      },
    };
  });
  return {
    paths,
    fallback: false,
  };
};

export async function getStaticProps(context) {
    const id = context.params.blogId;
    const [blogTitle, relatedBlog] = await Promise.all([
      fetch(`https://jsonplaceholder.typicode.com/photos/${id}`), 
      fetch('https://jsonplaceholder.typicode.com/photos?_page=1&_limit=10')
    ]);
    const [bTop, bSlide] = await Promise.all([
        blogTitle.json(), 
        relatedBlog.json()
    ]);
    return { props: { bTop, bSlide } };
}

/**/

function BlogDetail({ bTop, bSlide}) {

    const settings = {
        dots: false,
        arrows:true,
        autoplay:false,
        lazyLoad: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 2,		
    };

  //const [data, setData] = useState([]);

  /*Comment*/
  const [items, setItems] = useState([]);
  const [pageCount, setpageCount] = useState(0);
  const [isLoading, setIsLoading] = useState(true);

  let limit = 2;

  useEffect(() => {
      const getComments = async () => {
        const res = await fetch(
          `https://jsonplaceholder.typicode.com/posts/1/comments?_page=1&_limit=${limit}`

        );
        const data = await res.json();

        setIsLoading(false);

        const total = res.headers.get("x-total-count");
        setpageCount(Math.ceil(total / limit));
        // console.log(Math.ceil(total/12));
        setItems(data);
      };
  
      getComments();
  }, [limit]);
  
  const fetchComments = async (currentPage) => {
      const res = await fetch(
        `https://jsonplaceholder.typicode.com/posts/1/comments?_page=${currentPage}&_limit=${limit}`
      );
      const data = await res.json();
      return data;
  };
  
  const handlePageClick = async (data) => {
      console.log(data.selected);
      let currentPage = data.selected + 1;
      const commentsFormServer = await fetchComments(currentPage);
      setItems(commentsFormServer);
  }; 

  let { t } = useTranslation();

  return(
    <div className='container'>	
		  
			<h1>{bTop.title}</h1>
			<p>user:  {bTop.id}</p>		  
        <p><Image src="https://via.placeholder.com/1300x450/0000FF/808080%20?Text=Blog detail" alt="img" width="1300" height="450" /></p>
		    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard . </p>  
			
     

     {/*======  Comment section =========*/}
     <SkeletonTheme highlightColor="#aaa">
				
        <h3><strong>{t("common:comment")} </strong></h3>		
        {
            isLoading ?
            <>
            <SkeletonComment /><SkeletonComment /> </>
            :
            items.map((items) => {
                return (
                    <div key={items.id}>
                        <div className={styles.commentRow}>
                            <div className={styles.commentList}>
                                <strong>{t("common:name")} : </strong> {items.name}
                            </div>
                            <div className={styles.commentList}>
                                <strong>{t("common:email")} : </strong> {items.email}
                            </div>
                            <div className={styles.commentList}>
                                <p>{items.body}</p>
                            </div>
                        </div>
                    </div>
                );
            })
            
        }
            
        <ReactPaginate
            previousLabel={"previous"}
            nextLabel={"next"}
            breakLabel={"..."}
            pageCount={pageCount}
            marginPagesDisplayed={2}
            pageRangeDisplayed={3}
            onPageChange={handlePageClick}
            containerClassName={"pagination justify-content-center"}
            pageClassName={"page-item"}
            pageLinkClassName={"page-link"}
            previousClassName={"page-item"}
            previousLinkClassName={"page-link"}
            nextClassName={"page-item"}
            nextLinkClassName={"page-link"}
            breakClassName={"page-item"}
            breakLinkClassName={"page-link"}
            activeClassName={"active"}
        />      
    
    </SkeletonTheme>

     

			{/* Relate data ==================== */}
      <h2>{t("common:related_blog")}</h2>

      <div className="slider-wrap">
            <Slider {...settings}>	
                {                            
                  bSlide.map((user)=>{
                      return(
                          <div key={user.id} className='p-2'>
                              <div className='blogItem'>
                                    <div className="blogImage">
                                        <Link href={'/blog/' + user.id}><a>
                                            <Image src={user.url} alt='img' height="300" width="320"/>
                                        </a></Link>
                                    </div> 
                                    <div className='blog-content'>
                                        <h3 className='blog-title'>
                                            <Link href={'/blog/' + user.id}>{user.title}</Link>
                                        </h3>
                                        <p>User {user.id}</p>
                                    </div>    
                                </div>
                          </div>
                      )
                  })                        
                }	
            </Slider> 
      </div>
      
    </div>
  ) 
}

export default BlogDetail;

