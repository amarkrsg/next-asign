import React from "react";
import Slider from "react-slick";
import Image from 'next/image';


const BannerSlider = () => {
    const settings = {
        dots: false,
        arrows:true,
        autoplay:true,
        lazyLoad: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
      };
    return(
        <div className="mb-3">

          <Slider {...settings}>
            <div>
                <div>
                  <Image src="/banner1.png" alt='Banner' width="1800" height="450" />
                </div>
            </div>
            <div>
                <div>
                <Image src="/banner2.png" alt='Banner' width="1800" height="450" />
                </div>
            </div>
            <div>
                <div>
                <Image src="/banner3.png" alt='Banner' width="1800" height="450" />
                </div>
            </div>
            <div>
                <div>
                <Image src="/banner4.png" alt='Banner' width="1800" height="450" />
                </div>
            </div>          
          </Slider>
        </div>
    )
}

export default BannerSlider;

