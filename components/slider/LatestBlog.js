import Link from "next/link";
import Image from 'next/image';

function LatestBlog({user}){
    return(
        <>            
            <div className='blogItem'>
                <div className="blogImage">
                    <Link href={'/blog/' + user.id}><a>
                        <Image src={user.url} alt='img' width="320" height="300"/>
                    </a></Link>
                </div> 
                <div className='blog-content'>
                    <h3 className='blog-title'>
                        <Link href={'/blog/' + user.id}>{user.title}</Link>
                    </h3>
                    <p>User {user.id}</p>
                </div>    
            </div>       
        </>
    )
}

export default LatestBlog;
