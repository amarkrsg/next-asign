import Link from "next/link";
import Navbar from "./Navbar";
import { Fragment } from "react";
import Image from 'next/image';
import Auth from "./Auth";
import Language  from "./Language";
import styles from "../../styles/Header.module.css";

const Header = () => {
  return (
    <header className={styles.header}>
      <div className="container-fluid">
        <div className={styles.positionGroup}>
          <div className={styles.fCol1}>
          
              <Link href="/"><a><Image src='/logo.svg' className="App-logo" alt="logo" width="150" height='50' /></a></Link>
                           
          </div>

          <Navbar  />
          <div className="langNav">
            <Fragment>
              <Auth />
              <Language /> 
            </Fragment>
          </div>
          
          </div>
        </div>
    </header>
  );
};

export default Header;
