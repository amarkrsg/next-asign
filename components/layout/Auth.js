
import classes from './Login.module.css';
import React, {useState, useEffect} from "react";

import useTranslation from "next-translate/useTranslation";


const Auth = (props) => {

    const [userName,setUsername] = useState('');
    const [userIsValid, setUserIsValid] = useState();
    const [password,setPassword] = useState('');
    const [passwordIsValid, setPasswordIsValid] = useState();
    const [formIsValid, setFormIsValid] = useState(false);
     
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [isUser, setIsUser] = useState('');
    
    useEffect(() => {
        const identifier = setTimeout(() => {
          console.log('Checking form validity!');
          setFormIsValid(
            userName.trim().length > 4 && password.trim().length > 6
          );
        }, 500);
    
        return () => {
          console.log('CLEANUP');
          clearTimeout(identifier);
        };
    }, [userName, password]);
    const validateUserHandler = () => {
        setUserIsValid(userName.trim().length > 4);
      };
    const validatePasswordHandler = () => {
        setPasswordIsValid(password.trim().length > 6);
    };

    const submitForm = (event) => {
        event.preventDefault();
        //loginHandler(userName, password);

        loginHandler();
        localStorage.setItem('isUser', isUser);
             
        console.log(isUser);
        setUsername(''); setPassword('');
        //console.log(userName);
    }
    const userNameHandler = (event) => {
        setUsername(event.target.value);
        setIsUser(event.target.value);
    }
    const passwordHandler = (event) => {
        setPassword(event.target.value);
    }   

    useEffect(() => {
        const storedUserLoggedInInformation = localStorage.getItem('isLoggedIn');
        console.log(storedUserLoggedInInformation);
        if (storedUserLoggedInInformation === '1') {
            setIsLoggedIn(true);
        }
    }, []);
    const loginHandler = (user, password) => {
        if(formIsValid) {
            localStorage.setItem('isLoggedIn', '1');
            setIsLoggedIn(true);
        }        
    };
    const logoutHandler = () => {
        localStorage.removeItem('isLoggedIn');
        setIsLoggedIn(false);
        localStorage.removeItem('isUser');
    };

    /* Translation */
    let { t } = useTranslation();

    return(
        <>     
                   
            {!isLoggedIn && 
                <span className="login pointer" data-bs-toggle="modal" data-bs-target="#myModal">{t("common:login")} </span>
            }
            {isLoggedIn &&     
                <span>{localStorage.getItem('isUser')}</span>
            }
            {isLoggedIn &&            
                <span className="logout pointer" onClick={logoutHandler}>
                {t("common:logout")}</span>
            }

         
            {/* {error && <ErrorModal title={error.title} 
            message={error.message} />}  */}
           
          
            <div className="modal" id="myModal">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">                  
                        <div className="modal-header">
                            <h4 className="modal-title">{t("common:login")}</h4>
                            <button type="button" className="btn-close" data-bs-dismiss="modal"></button>
                        </div>

                        <div className="modal-body">
                            <form onSubmit={submitForm}  autoComplete='off'>
                                <div className={`${classes.formGroup} ${
                                    userIsValid === false ? classes.invalid : ''
                                }`}>
                                    <label htmlFor="email">{t("common:user_name")}</label>
                                    <input className="form-control" 
                                    autoComplete="off" 
                                    type="text"
                                    id="email"
                                    value={userName}
                                    onChange={userNameHandler}
                                    onBlur={validateUserHandler}
                                    />
                                    {!userIsValid ? 
                                    <p className={classes.invalid}>Please enter at lest 5 digit</p> :""}                           
                                </div>
                                
                                
                                <div className={`${classes.formGroup} ${
                                    passwordIsValid === false ? classes.invalid : ''
                                }`}>
                                    <label htmlFor="password">{t("common:password")}</label>
                                    <input className="form-control" autoComplete='off' 
                                    type="password"
                                    id="password"
                                    value={password}
                                    onChange={passwordHandler}
                                    onBlur={validatePasswordHandler}
                                    />
                                </div>
                                <div className="d-flex justify-content-between">   
                                                    
                                    <button type="submit" 
                                    className="btn btn-primary mr-3" disabled={!formIsValid} data-bs-dismiss="modal">
                                        {t("common:save")} 
                                    </button>
                                    <button className="btn btn-danger" data-bs-dismiss="modal">
                                    {t("common:close")}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

           
        </>
        
    )
}



export default Auth;