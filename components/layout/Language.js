
import Link from 'next/link';
import { useRouter } from "next/router";


import useTranslation from "next-translate/useTranslation";




const Language = () => { 
    let router = useRouter(); 
    let { t } = useTranslation();
    return(
        <>  
            <Link href={router.asPath} locale="en">{t("common:en")}</Link>
            <Link href={router.asPath} locale="th">{t("common:th")}</Link>
            
            {/* <span>Lang {router.locale}</span> */}
            {/* <ul>
                {router.locales.map((locale) => (
                    <li key={locale}>
                        <Link href={router.asPath} locale={locale}>
                            <a>{locale}</a>
                        </Link>
                    </li>
                ))}
            </ul> */}
        </>
        
    )
}

export default Language; 


