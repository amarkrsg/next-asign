import Link from "next/link";
import useTranslation from "next-translate/useTranslation";

const Navbar = () => {
  let { t } = useTranslation();
  return (
    <>
      <nav>
        <ul className="menu-bar">
          <li>
            <Link href="/">
              <a> {t("common:home")} </a>
            </Link>
          </li>
          <li>
            <Link href="/blog">
              <a> {t("common:blog")} </a>
            </Link>
          </li>
          <li>
            <Link href="/contact">
              <a> {t("common:contact")} </a>
            </Link>
          </li>
          
        </ul>
      </nav>
    </>
  );
};

export default Navbar;
