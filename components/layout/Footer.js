import Navbar from "./Navbar";
import styles from "../../styles/Footer.module.css";
import useTranslation from "next-translate/useTranslation";

const Footer = () => {
  let { t } = useTranslation();
  return (
    <footer className={styles.footer}>
        <h3>{t("common:footer")} </h3>
        <Navbar />
    </footer>
  );
};

export default Footer;
