const nextTranslate = require("next-translate");

module.exports = {
  ...nextTranslate(),
  reactStrictMode: true,
  
  images: {
    domains: ['via.placeholder.com','jsonplaceholder.typicode.com/photos'],
  },
 
}  
